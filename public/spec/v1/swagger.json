{
    "swagger": "2.0",
    "info": {
        "version": "2.5.0",
        "title": "TACC API"
    },
    "host": "tas.tacc.utexas.edu",
    "basePath": "/api/v1",
    "schemes": [
        "https"
    ],
    "securityDefinitions": {
        "basic_auth": {
            "type": "basic"
        }
    },
    "security": [
        {
            "basic_auth": []
        }
    ],
    "produces": [
        "application/json"
    ],
    "tags": [
        {
            "name": "users",
            "description": "Users"
        },
        {
            "name": "projects",
            "description": "Projects"
        },
        {
            "name": "allocations",
            "description": "Allocations"
        }
    ],
    "paths": {
        "/v1/users": {
            "post": {
                "tags": [
                    "users"
                ],
                "description": "Create a new `UserProfile`",
                "parameters": [
                    {
                        "name": "body",
                        "in": "body",
                        "schema": {
                            "$ref": "#/definitions/UserProfile"
                        },
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Ticket created",
                        "schema": {
                            "$ref": "#/definitions/UserProfile"
                        }
                    }
                }
            }
        },
        "/v1/users/{id}": {
            "get": {
                "tags": [
                    "users"
                ],
                "description": "Get a `UserProfile`",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            },
            "put": {
                "tags": [
                    "users"
                ],
                "description": "Update a `UserProfile`",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    },
                    {
                        "name": "body",
                        "in": "body",
                        "description": "The updated `UserProfile` object",
                        "schema": {
                            "$ref": "#/definitions/UserProfile"
                        },
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/users/username/{username}": {
            "get": {
                "tags": [
                    "users"
                ],
                "description": "Get a `UserProfile`",
                "parameters": [
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/users/{id}/{code}": {
            "put": {
                "tags": [
                    "users"
                ],
                "description": "Confirms a user's email",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    },
                    {
                        "name": "code",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/users/{username}/passwordResets": {
            "post": {
                "tags": [
                    "users"
                ],
                "description": "Requests a password reset",
                "parameters": [
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/users/{username}/passwordResets/{code}": {
            "post": {
                "tags": [
                    "users"
                ],
                "description": "Resets a password using a previously requested reset code",
                "parameters": [
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    },
                    {
                        "name": "code",
                        "in": "path",
                        "type": "string",
                        "required": true
                    },
                    {
                        "name": "password",
                        "in": "query",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/users/{username}/passwordChanges": {
            "post": {
                "tags": [
                    "users"
                ],
                "description": "Changes a password using the current password",
                "parameters": [
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    },
                    {
                        "name": "password",
                        "in": "query",
                        "type": "string",
                        "required": true
                    },
                    {
                        "name": "newPassword",
                        "in": "query",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/users/institutions": {
            "get": {
                "tags": [
                    "users"
                ],
                "description": "Get a list of `Institution`",
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/institutions/{id}/departments": {
            "get": {
                "tags": [
                    "users"
                ],
                "description": "Get a list of `Department` for a `Institution`",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    },
                    "404": {
                        "description": "No `State`s for `Country`"
                    }
                }
            }
        },
        "/v1/users/countries": {
            "get": {
                "tags": [
                    "users"
                ],
                "description": "Get a list of `Country`",
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/users/countries/{countryId}/states": {
            "get": {
                "tags": [
                    "users"
                ],
                "description": "Get a list of `State` for a `Country`",
                "parameters": [
                    {
                        "name": "countryId",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    },
                    "404": {
                        "description": "No `State`s for `Country`"
                    }
                }
            }
        },
        "/v1/users/{username}/dn": {
            "get": {
                "tags": [
                    "users"
                ],
                "description": "Get DN list for `UserProfile`",
                "parameters": [
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    }
                }
            },
            "post": {
                "tags": [
                    "users"
                ],
                "description": "Add a DN to the User",
                "parameters": [
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    },
                    {
                        "name": "dn",
                        "in": "body",
                        "schema": {
                            "type": "string"
                        },
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Pairing created"
                    }
                }
            },
            "delete": {
                "tags": [
                    "users"
                ],
                "description": "Add a DN to the User",
                "parameters": [
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    },
                    {
                        "name": "dn",
                        "in": "body",
                        "schema": {
                            "type": "string"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Pairing created"
                    }
                }
            }
        },
        "/v1/projects": {
            "post": {
                "tags": [
                    "projects"
                ],
                "description": "Create a new `Project`",
                "parameters": [
                    {
                        "name": "body",
                        "in": "body",
                        "schema": {
                            "$ref": "#/definitions/UserProfile"
                        },
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Project created",
                        "schema": {
                            "$ref": "#/definitions/Project"
                        }
                    }
                }
            }
        },
        "/v1/projects/{id}": {
            "get": {
                "tags": [
                    "projects"
                ],
                "description": "Get `Project`",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "404": {
                        "description": ""
                    }
                }
            },
            "put": {
                "tags": [
                    "users"
                ],
                "description": "Update a `Project`",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    },
                    {
                        "name": "body",
                        "in": "body",
                        "description": "The updated `UserProfile` object",
                        "schema": {
                            "$ref": "#/definitions/Project"
                        },
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/projects/username/{username}": {
            "get": {
                "tags": [
                    "projects"
                ],
                "description": "Get a list of `Project` associated with the given user",
                "parameters": [
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "404": {
                        "description": ""
                    }
                }
            }
        },
        "/v1/projects/groups/{group}": {
            "get": {
                "tags": [
                    "projects"
                ],
                "description": "Get a list of `Project` associated with the given project group",
                "parameters": [
                    {
                        "name": "group",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "404": {
                        "description": ""
                    }
                }
            }
        },
        "/v1/projects/groups/{group}/status/{status}": {
            "get": {
                "tags": [
                    "projects"
                ],
                "description": "Get a list of `Project` associated with the given project group. allocation status",
                "parameters": [
                    {
                        "name": "group",
                        "in": "path",
                        "type": "string",
                        "required": true
                    },
                    {
                        "name": "status",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "404": {
                        "description": ""
                    }
                }
            }
        },
        "/v1/projects/{id}/users": {
            "get": {
                "tags": [
                    "projects"
                ],
                "description": "Get list of project users",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "404": {
                        "description": ""
                    }
                }
            }
        },
        "/v1/projects/{id}/users/{username}": {
            "post": {
                "tags": [
                    "projects"
                ],
                "description": "Add a user to a project",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    },
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Pairing created"
                    }
                }
            },
            "delete": {
                "tags": [
                    "projects"
                ],
                "description": "Remove a user from a project",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    },
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Pairing created"
                    }
                }
            }
        },
        "/v1/projects/{projectId}/allocations/{id}": {
            "put": {
                "tags": [
                    "users"
                ],
                "description": "Update an allocation on a given project",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    },
                    {
                        "name": "projectId",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    },
                    {
                        "name": "body",
                        "in": "body",
                        "description": "The updated `Allocation` object",
                        "schema": {
                            "$ref": "#/definitions/Allocation"
                        },
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Successful"
                    }
                }
            }
        },
        "/v1/allocations/{id}/users/{username}": {
            "post": {
                "tags": [
                    "allocations"
                ],
                "description": "Add a user to an allocation",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    },
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Pairing created"
                    }
                }
            },
            "delete": {
                "tags": [
                    "allocations"
                ],
                "description": "Remove a user from an allocation",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    },
                    {
                        "name": "username",
                        "in": "path",
                        "type": "string",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Pairing created"
                    }
                }
            }
        },
        "/v1/allocations/{id}": {
            "get": {
                "tags": [
                    "allocations"
                ],
                "description": "Get `Allocation`",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "type": "integer",
                        "format": "int32",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": ""
                    },
                    "404": {
                        "description": ""
                    }
                }
            }
        }
    },
    "definitions": {
        "UserProfile": {
            "type": "object",
            "required": [
                "username",
                "email",
                "firstName",
                "lastName",
                "institutionId",
                "countryId"
            ],
            "properties": {
                "id": {
                    "type": "integer",
                    "format": "int32"
                },
                "uteid": {
                    "type": "integer",
                    "format": "int32"
                },
                "username": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "firstName": {
                    "type": "string"
                },
                "middleInitial": {
                    "type": "string"
                },
                "lastName": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                },
                "phone": {
                    "type": "string"
                },
                "address": {
                    "type": "string"
                },
                "city": {
                    "type": "string"
                },
                "stateId": {
                    "type": "integer",
                    "format": "int32"
                },
                "zip": {
                    "type": "string"
                },
                "countryId": {
                    "type": "integer",
                    "format": "int32"
                },
                "institutionId": {
                    "type": "integer",
                    "format": "int32"
                },
                "departmentId": {
                    "type": "integer",
                    "format": "int32"
                },
                "piEligibility": {
                    "type": "boolean"
                },
                "emailFormat": {
                    "type": "string"
                },
                "staff": {
                    "type": "boolean",
                    "readOnly": true
                }
            }
        },
        "Project": {
            "type": "object",
            "required": [
                "title",
                "description",
                "chargeCode",
                "gid",
                "source",
                "fieldId",
                "typeId",
                "piId"
            ],
            "properties": {
                "title": {
                    "type": "string"
                },
                "description": {
                    "type": "string"
                },
                "chargeCode": {
                    "type": "string"
                },
                "source": {
                    "type": "string"
                },
                "gid": {
                    "type": "integer",
                    "format": "int32"
                },
                "fieldId": {
                    "type": "integer",
                    "format": "int32"
                },
                "typeId": {
                    "type": "integer",
                    "format": "int32"
                },
                "piId": {
                    "type": "integer",
                    "format": "int32"
                }
            }
        },
        "Allocation": {
            "type": "object",
            "required": [
                "accountId",
                "requested",
                "justification",
                "resourceId"
            ],
            "properties": {
                "accountId": {
                    "type": "integer",
                    "format": "int32"
                },
                "requested": {
                    "type": "integer",
                    "format": "int32"
                },
                "justification": {
                    "type": "string"
                },
                "resourceId": {
                    "type": "integer",
                    "format": "int32"
                }
            }
        }
    }
}